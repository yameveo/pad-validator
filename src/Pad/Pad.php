<?php
/**
 * Created by PhpStorm.
 * User: yameveo
 * Date: 08/06/2017
 * Time: 14:32
 */

namespace Pad;

use Pad\Exception\PadParseException;
use Pad\Exception\PadValidationException;

class Pad
{
    /**
     * @var string
     */
    protected $standardUrl;

    /**
     * Pad constructor.
     * @param $standardUrl
     */
    public function __construct($standardUrl)
    {
        $this->standardUrl = $standardUrl;
    }

    /**
     * Parse PAD from URL
     * @param $url
     * @return PadFile
     * @throws PadParseException
     */
    public function parseFromUrl($url)
    {
        $pad = new PadFile($url);
        $pad->Load();
        switch($pad->LastError) {
            case XMLFile::ERR_NO_URL_SPECIFIED:
            case XMLFile::ERR_READ_FROM_URL_FAILED:
            case XMLFile::ERR_PARSE_ERROR:
                throw new PadParseException($pad->LastErrorMsg);
                break;
        }
        return $pad;
    }

    /**
     * Parse PAD XML
     * @param $data
     * @return PadFile
     * @throws PadParseException
     */
    public function parseRaw($data)
    {
        $pad = new PadFile();
        if(!$pad->Parse($data)) {
            throw new PadParseException($pad->ParseError);
        }
        return $pad;
    }

    /**
     * Validates a pad content VS a pad sta
     * @param $pad
     * @return bool
     * @throws PadValidationException
     */
    public function validate($pad)
    {
        $validator = new PADValidator($this->standardUrl);
        $validator->Load();
        $errors = $validator->Validate($pad);
        if($errors) {
            throw new PadValidationException($errors);
        }
        return true;
    }
}