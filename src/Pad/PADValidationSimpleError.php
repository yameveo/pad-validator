<?php
/**
 * Created by PhpStorm.
 * User: yameveo
 * Date: 08/06/2017
 * Time: 12:21
 */

namespace Pad;


class PADValidationSimpleError extends PADValidationError
{
    //////////////////////////////////////////////////////////////////////////////
    // Public Properties
    //////////////////////////////////////////////////////////////////////////////

    var $SpecFieldNode;
    var $Value;
    var $ErrorText;


    //////////////////////////////////////////////////////////////////////////////
    // Construction
    //////////////////////////////////////////////////////////////////////////////

    // Constructor
    // IN: &$PADValidator - reference to the PADValidator object holding this error
    // IN: &$SpecFieldNode - reference to the XMLNode object holding the field spec
    // IN: $Value          - current value of the field
    function PADValidationSimpleError($PADValidator, $SpecFieldNode, $Value, $ErrorText)
    {
        // Inherited
        parent::PADValidationError($PADValidator);

        $this->SpecFieldNode = $SpecFieldNode;
        $this->Value = $Value;

        $this->ErrorText = $ErrorText;
    }


    //////////////////////////////////////////////////////////////////////////////
    // Methods
    //////////////////////////////////////////////////////////////////////////////

    // Dump error to HTML
    function Dump()
    {
        $this->DumpValue($this->SpecFieldNode->GetValue("Name"), $this->Value);
        $this->DumpError($this->SpecFieldNode->GetValue("Title"), $this->ErrorText, "");
    }
}