<?php
/**
 * Created by PhpStorm.
 * User: yameveo
 * Date: 08/06/2017
 * Time: 12:24
 */

namespace Pad;

class PADValidator extends PadSpec
{
    //////////////////////////////////////////////////////////////////////////////
    // Public Properties
    //////////////////////////////////////////////////////////////////////////////

    var $ValidationErrors;


    //////////////////////////////////////////////////////////////////////////////
    // Construction
    //////////////////////////////////////////////////////////////////////////////

    // Constructor
    // IN: $URL - the URL or local path of the PAD spec file (optional)
    function __construct($URL = "")
    {
        // Inherited
        parent::PADSpec($URL);

        // Inits
        $this->ValidationErrors = array();
    }


    function Validate(&$PADFile)
    {
        // Clear
        $this->ValidationErrors = array();

        // Verify URL against Application_XML_File_URL
        $PADFieldValue = $PADFile->XML->GetValue("XML_DIZ_INFO/Web_Info/Application_URLs/Application_XML_File_URL");
        $SpecFieldNode = $this->FindFieldNode("XML_DIZ_INFO/Web_Info/Application_URLs/Application_XML_File_URL");

        if ( strcasecmp($PADFile->URL, $PADFieldValue) != 0 )
            new PADValidationSimpleError($this, $SpecFieldNode, $PADFieldValue, "does not match the URL you entered.");

        // Parsing the PAD URL
        $host = parse_url($PADFile->URL, PHP_URL_HOST);

        // Verify that PAD comes from an authorized source
        if ( strcasecmp($host, 'repository.appvisor.com') != 0 )
            new PADValidationSimpleError( $this, $SpecFieldNode, $PADFieldValue, "This PAD comes from an unauthorized source. Please make sure to publish it in <a href=\"http://repository.appvisor.com?ref=pad_validation_error\"
 target=\"_blank\">the Official PAD Repository</a>. According to the PAD Specification ver.4,
      all PAD files are hosted in <a href=\"http://repository.appvisor.com?ref=pad_validation_error\" target=\"_blank\">the Official PAD
      Repository</a> for security reasons. This ensures the validity of PAD files and protects download sites from PAD Spam.");

        // Return number of errors
        return count($this->ValidationErrors);
    }

    function ValidateRegEx(&$PADFile)
    {
        // Clear
        $this->ValidationErrors = array();


        // Verify RegEx: Walk over all fields in the spec
        foreach($this->FieldsNode->ChildNodes as $SpecFieldNode)
        {
            // Get Path and RegEx for this field
            $Path = $SpecFieldNode->GetValue("Path");
            $RegEx = $SpecFieldNode->GetValue("RegEx");

            // Find the field content in the PAD
            $PADFieldValue = $PADFile->XML->GetValue($Path);

            // Match against the RegEx
            if ( !preg_match("/" . $RegEx . "/", $PADFieldValue) )
                new PADValidationRegExError($this, $SpecFieldNode, $PADFieldValue);
        }


        // Verify descriptions in languages other than English
        $DescrFieldNames = array("Keywords", "Char_Desc_45", "Char_Desc_80",
            "Char_Desc_250", "Char_Desc_450", "Char_Desc_2000");
        $NodeDescriptions =& $PADFile->XML->FindNodeByPath("XML_DIZ_INFO/Program_Descriptions");
        if ($NodeDescriptions)
            foreach($NodeDescriptions->ChildNodes as $DescrNode)
                if ( $DescrNode->Name != "English" )
                    foreach($DescrFieldNames as $DescrFieldName)
                    {
                        // Find the spec field (with English instead of this language)
                        $Path = "XML_DIZ_INFO/Program_Descriptions/" . $DescrNode->Name . "/" . $DescrFieldName;
                        $SpecFieldNode = $this->FindFieldNode("XML_DIZ_INFO/Program_Descriptions/English/" . $DescrFieldName);

                        // Overriding the Path child node does not work yet, so it will always show 'English'
                        //$NodePath = $SpecFieldNode->FindNodeByName("Path");
                        //$NodePath->Value = $Path;

                        // Get RegEx for this field
                        $RegEx = $SpecFieldNode->GetValue("RegEx");

                        // Find the field content in the PAD
                        $PADFieldValue = $PADFile->XML->GetValue($Path);

                        // Match against the RegEx
                        if ( !preg_match("/" . $RegEx . "/", $PADFieldValue) )
                            new PADValidationRegExError($this, $SpecFieldNode, $PADFieldValue);
                    }


        // Verify URL against Application_XML_File_URL
        $PADFieldValue = $PADFile->XML->GetValue("XML_DIZ_INFO/Web_Info/Application_URLs/Application_XML_File_URL");
        $SpecFieldNode = $this->FindFieldNode("XML_DIZ_INFO/Web_Info/Application_URLs/Application_XML_File_URL");
        if ( strcasecmp($PADFile->URL, $PADFieldValue) != 0 )
            new PADValidationSimpleError($this, $SpecFieldNode, $PADFieldValue, "does not match the URL you entered.");


        // Return number of errors
        return count($this->ValidationErrors);
    }
}