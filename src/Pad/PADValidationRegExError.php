<?php
/**
 * Created by PhpStorm.
 * User: yameveo
 * Date: 08/06/2017
 * Time: 12:21
 */

namespace Pad;


class PADValidationRegExError extends PADValidationError
{
    //////////////////////////////////////////////////////////////////////////////
    // Public Properties
    //////////////////////////////////////////////////////////////////////////////

    var $SpecFieldNode;
    var $Value;


    //////////////////////////////////////////////////////////////////////////////
    // Construction
    //////////////////////////////////////////////////////////////////////////////

    // Constructor
    // IN: &$PADValidator - reference to the PADValidator object holding this error
    // IN: &$SpecFieldNode - reference to the XMLNode object holding the field spec
    // IN: $Value          - current value of the field
    function PADValidationRegExError($PADValidator, $SpecFieldNode, $Value)
    {
        // Inherited
        parent::PADValidationError($PADValidator);

        $this->SpecFieldNode = $SpecFieldNode;
        $this->Value = $Value;
    }


    //////////////////////////////////////////////////////////////////////////////
    // Methods
    //////////////////////////////////////////////////////////////////////////////

    // Dump error to HTML
    function Dump()
    {
        $this->DumpValue($this->SpecFieldNode->GetValue("Name"), $this->Value);
        $this->DumpError($this->SpecFieldNode->GetValue("Title"),
            "does not match specification:",
            $this->SpecFieldNode->GetValue("RegExDocumentation"));
    }
}