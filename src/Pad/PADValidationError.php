<?php
/**
 * Created by PhpStorm.
 * User: yameveo
 * Date: 08/06/2017
 * Time: 12:20
 */

namespace Pad;


class PADValidationError
{
    //////////////////////////////////////////////////////////////////////////////
    // Construction
    //////////////////////////////////////////////////////////////////////////////

    // Constructor
    // IN: &$PADValidator - reference to the PADValidator object holding this error
    function PADValidationError($PADValidator)
    {
        // Append this error to the array
        array_push($PADValidator->ValidationErrors, $this);
    }

    //////////////////////////////////////////////////////////////////////////////
    // Methods
    //////////////////////////////////////////////////////////////////////////////

    // Dump error to HTML
    function Dump()
    {
        echo "Unknown Error.";
    }

    // Dump a name/value pair
    // IN: $Name  - the name
    // IN: $Value - the corresponding value
    function DumpValue($Name, $Value)
    {
        echo "<pre><b>" . htmlspecialchars($Name . ":") . "</b> ";
        if ( $Value == "" )
            echo "<i>(empty)</i>";
        else
            echo htmlspecialchars($Value);
        echo "</pre>";
    }

    // Dump an error string
    // IN: $Title - the title
    // IN: $Text  - the error text
    // IN: $Descr - the error description
    function DumpError($Title, $Text, $Descr)
    {
        echo "<b>" . "Invalid " . htmlspecialchars($Title) . "</b> " .
            "<br/>" . $Text . " " .
            "<i>" . $Descr . "</i>";
    }
}