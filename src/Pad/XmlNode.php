<?php
/**
 * Created by PhpStorm.
 * User: yameveo
 * Date: 08/06/2017
 * Time: 12:17
 */

namespace Pad;

class XmlNode
{
    //////////////////////////////////////////////////////////////////////////////
    // Public Properties
    //////////////////////////////////////////////////////////////////////////////

    var $Name;
    var $Value = "";
    var $ParentNode;
    var $ChildNodes;
    var $Level = 0;


    //////////////////////////////////////////////////////////////////////////////
    // Construction
    //////////////////////////////////////////////////////////////////////////////

    // Constructor
    function __construct($Name)
    {
        // Initializations
        $this->Name = $Name;
        $this->ChildNodes = array();
    }


    //////////////////////////////////////////////////////////////////////////////
    // Public Methods
    //////////////////////////////////////////////////////////////////////////////

    // Set parent node
    // IN: &$ParentNode - reference to the new parent node
    function SetParent(&$ParentNode)
    {
        $this->ParentNode = &$ParentNode;
        $this->Level = $this->ParentNode->Level + 1;
    }


    // Clear contents
    function Clear()
    {
        $this->Name = "";
        $this->ChildNodes = array();
        unset($this->ParentNode);
        $this->Level = 0;
    }

    // Append a node
    // IN:      $Name - the tag name of the node to add
    // RETURNS: reference to the XMLNode object that has been created
    function &AppendNode($Name)
    {
        $node = new XMLNode($Name);
        $node->SetParent($this);

        // Do not use array_push with pass-by-reference any more to avoid
        // allow_call_time_pass_reference warning (Oliver Grahl, 2006-08-31)
        //array_push($this->ChildNodes, &$node);
        $this->ChildNodes[] =& $node;

        return $node;
    }

    // Returns the node according to the path (/-separated)
    // IN:      $Path  - the path to the XML node, e.g. Root/Child/Name
    // RETURNS: reference to the XMLNode object, NULL if node is not found
    function &FindNodeByPath($Path)
    {
        // To make PHP5 happy, we will not return NULL, but a variable which
        // has a value of NULL.
        $NULL = NULL;

        $tags = explode("/", $Path);

        if ( count($tags) <= 0 )
            return $NULL;

        $node =& $this;
        foreach($tags as $tag) {
            if (($tag != "") && !($node =& $node->FindNodeByName($tag)))
                return $NULL;
        }

        return $node;
    }

    // Returns a node value according to the path (/-separated)
    // IN:      $Path    - the path to the XML node, e.g. Root/Child/Name
    // IN:      $Default - value to use if node does not exists (optional)
    // RETURNS: value of the node as string, empty string or default value if
    //          node is not found
    function GetValue($Path, $Default = "")
    {
        $node =& $this->FindNodeByPath($Path);
        if ( $node )
            return $node->Value;
        else
            return $Default;
    }

    // Transforms XML tree to XML text
    // RETURNS: the XML string
    function ToString()
    {
        if ( $this->Level == 0 )
        {
            // This is the root node, only walk through children
            $out = "";
            foreach($this->ChildNodes as $node)
                $out .= $node->ToString();
        }
        else
        {
            // Print node depending of it's type
            $indent = str_repeat("\t", $this->Level - 1);

            if ( count($this->ChildNodes) <= 0 )
            {
                // A node without children
                if ( $this->Value == "" )
                    $out = $indent . "<" . $this->Name . " />\n";
                else
                    $out = $indent . "<" . $this->Name . ">" . $this->Value . "</" . $this->Name . ">\n";
            }
            else
            {
                // A node with children
                $out = $indent . "<" . $this->Name . ">" . $this->Value . "\n";
                foreach($this->ChildNodes as $node)
                    $out .= $node->ToString();
                $out .= $indent . "</" . $this->Name . ">\n";
            }
        }

        return $out;
    }

    // Dumps the XML text to HTML
    function Dump()
    {
        echo "<pre>" . htmlspecialchars($this->ToString()) . "</pre>";
    }

    // Returns the node according to the name
    // IN:      $Name  - the name of the XML child node, e.g. Child
    // RETURNS: reference to the XMLNode object, NULL if node is not found
    function &FindNodeByName($Name)
    {
        foreach($this->ChildNodes as $node) {
            //var_dump($node);
            if ( $node->Name == $Name ) {
                return $node;
            }
        }

        // To make PHP5 happy, we will not return NULL, but a variable which
        // has a value of NULL.
        $NULL = NULL;
        return $NULL;
    }
}