<?php

require __DIR__.'/../vendor/autoload.php';

$pad = new \Pad\Pad('http://repository.appvisor.com/padspec/files/padspec.xml');

$padFile = $pad->parseFromUrl('http://repository.appvisor.com/info/app-8d000647d3d6/AlphaPlugins_TurbulenceDistortion_Win_pad.xml');
var_dump($pad->validate($padFile));

$padFile = $pad->parseRaw(file_get_contents('http://repository.appvisor.com/info/app-8d000647d3d6/AlphaPlugins_TurbulenceDistortion_Win_pad.xml'));
var_dump($pad->validate($padFile));